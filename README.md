# bored-app

This SAM application contains the necessary code to return a get request from boredapi.com and return the output in a form easily integrated with AWS Lex.

src/app.py:lambda_handler contains the code to perform the work.
template.yaml is the SAM template that creates necessary resources in AWS.
