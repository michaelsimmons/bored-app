import json
import requests
from random import choice


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    AWS Lex Response
    """
    
    try:
        response = requests.get("http://www.boredapi.com/api/activity")
        
    except requests.RequestException as e:
        # Send some context about this error to Lambda Logs
        print(e)

        raise e

    data = response.json()
    activity = ""
    prefixes = ["How about you ", "You could", "Try to"]

    if 'error' in data:
        activity = "Sorry, I couldn\'t find an activity that matched your request."
    else:
        activity = f"{choice(prefixes)} {data['activity'].lower()}"

    # print(f'\n\n{activity}')
    return {
      "dialogAction": {
        "type": "Close",
        "fulfillmentState": "Fulfilled",
        "message": {
            "contentType": "PlainText",
            "content": activity
        },
      }
    }
